FROM postgres:14-bullseye as run
RUN apt-get update \
    && apt-get install -y python3-psycopg2 postgresql-plperl-14

COPY source/scripts/create_language.sh /docker-entrypoint-initdb.d

EXPOSE 5432
